#!/bin/bash

#------------------------------------------------------------
# How to use
# $ chmod u+x install-aws-sdk-php.sh
# $ ./install-aws-sdk-php.sh
#------------------------------------------------------------
curl -sS https://getcomposer.org/installer | php

php composer.phar require aws/aws-sdk-php
