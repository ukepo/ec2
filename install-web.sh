#!/bin/bash

#------------------------------------------------------------
# for Amazon Linux AMI 2015.03
#------------------------------------------------------------

#------------------------------------------------------------
# How to use
# $ sudo su -
# $ wget https://gitlab.com/ukepo/ec2/raw/master/install-web.sh
# $ chmod u+x install-web.sh
# $ ./install-web.sh
#------------------------------------------------------------

#------------------------------------------------------------
# Update yum
#------------------------------------------------------------
yum update -y

#------------------------------------------------------------
# Install gcc
#------------------------------------------------------------
sudo yum install -y gcc make gcc-c++

#------------------------------------------------------------
# Install Nginx, PHP5.5, PHP-FPM, DB
#------------------------------------------------------------
yum install nginx php55 php55-fpm php55-xml php55-pdo php55-odbc \
php55-soap php55-common php55-cli php55-mbstring php55-bcmath php55-ldap \
php55-imap php55-gd php55-pecl-apc -y

yum install mysql-server mysql php55-mysqlnd -y
yum install postgresql-server postgresql php55-pgsql -y

chkconfig nginx on
chkconfig mysqld on
chkconfig php55-fpm on

service mysqld start

sed -i 's/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm\.sock/' /etc/php-fpm.d/www.conf
sed -i 's/;listen\.owner = nobody/listen\.owner = nginx/' /etc/php-fpm.d/www.conf
sed -i 's/;listen\.group = nobody/listen\.group = nginx/' /etc/php-fpm.d/www.conf
sed -i 's/;listen\.mode = 0666/listen\.mode = 0664/' /etc/php-fpm.d/www.conf
sed -i 's/user = apache/user = nginx/' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/' /etc/php-fpm.d/www.conf

echo 'server {' >> /etc/nginx/conf.d/default.conf
echo '     location / {' >> /etc/nginx/conf.d/default.conf
echo '     root /usr/share/nginx/html;' >> /etc/nginx/conf.d/default.conf
echo '     index index.php index.html index.htm;' >> /etc/nginx/conf.d/default.conf
echo '     }' >> /etc/nginx/conf.d/default.conf
echo '     '>> /etc/nginx/conf.d/default.conf
echo '     location ~ \.php {' >> /etc/nginx/conf.d/default.conf
echo '     fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;' >> /etc/nginx/conf.d/default.conf
echo '     fastcgi_index index.php;' >> /etc/nginx/conf.d/default.conf
echo '     fastcgi_param SCRIPT_FILENAME /usr/share/nginx/html$fastcgi_script_name;' >> /etc/nginx/conf.d/default.conf
echo '     include fastcgi_params;' >> /etc/nginx/conf.d/default.conf
echo '     }' >> /etc/nginx/conf.d/default.conf
echo '}' >> /etc/nginx/conf.d/default.conf

service php-fpm start
service nginx start

#------------------------------------------------------------
# Install git and git-flow
#------------------------------------------------------------
yum install git -y
cd /usr/local/src
wget -q -O - --no-check-certificate https://github.com/nvie/gitflow/raw/develop/contrib/gitflow-installer.sh | bash

#------------------------------------------------------------
# Display message
#------------------------------------------------------------
echo 'you have to execute $mysql_secure_installation'
